# Webpack boilerplate 
just because i need to familiarize myself with it

### TODO
1. HTML
  1. [ ] Needs a boilerplate (html5boilerplate? questionmark)
  2. [ ] Minify
  3. [ ] placeholder
2. CSS
  1. [ ] Materialize css
  2. [ ] uncss
  3. [ ] Critical inline
  4. [ ] placeholder
3. Javascript
  1. [ ] Typescript > Babel?
  2. [ ] Minify
  3. [ ] Unglify
4. Images
  1. [ ] Optimize
  2. [ ] Scale
  3. [ ] Image swap or progressive loading?
5. Develop
  1. [ ] Hotswap
  2. [ ] Livereload